package com.capgemini.mrchecker.selenium.jsoup;

import java.util.List;

import org.openqa.selenium.By;

import com.capgemini.mrchecker.selenium.core.BasePage;
import com.capgemini.mrchecker.selenium.jsoupHelper.JsoupHelper;
import com.capgemini.mrchecker.test.core.logger.BFLogger;;

public class ChallengingDomPage extends BasePage {
	private static final By	selectorTable			= By.cssSelector("div.large-10.columns > table");
	private static final By	selectorHeaderTable		= By.cssSelector("div.large-10.columns>table>thead>tr>th");
	private static final By	selector4ColumnTable	= By.cssSelector("div.large-10.columns>table>tbody>tr>td:nth-child(4)");
	
	@Override
	public boolean isLoaded() {
		BFLogger.logDebug("isLoaded");
		BFLogger.logDebug(getDriver().getTitle());
		BFLogger.logDebug("Boolean: " + getDriver().getTitle()
				.equals(pageTitle()));
		return getDriver().getTitle()
				.equals(pageTitle());
	}
	
	@Override
	public void load() {
		BFLogger.logError("Search result page not loaded");
		BFLogger.logDebug("loading page");
		getDriver().get("https://the-internet.herokuapp.com/challenging_dom");
	}
	
	@Override
	public String pageTitle() {
		BFLogger.logDebug("Page title");
		return "The Internet";
	}
	
	public boolean isTablePresent() {
		return getDriver().findElementDynamic(selectorTable)
				.isDisplayed();
	}
	
	public List<String> getHeaderTableValues() {
		return JsoupHelper.findTexts(selectorHeaderTable);
	}
	
	public List<String> get4thColumnTableValues() {
		return JsoupHelper.findTexts(selector4ColumnTable);
	}
}
