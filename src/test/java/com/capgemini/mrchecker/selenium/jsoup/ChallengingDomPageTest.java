package com.capgemini.mrchecker.selenium.jsoup;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.capgemini.mrchecker.test.core.BaseTest;
import com.capgemini.mrchecker.test.core.logger.BFLogger;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class ChallengingDomPageTest extends BaseTest {
	
	private static ChallengingDomPage challengingDomPage;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		BFLogger.logInfo("Step1 - open https://the-internet.herokuapp.com/challenging_dom");
		challengingDomPage = new ChallengingDomPage();
	}
	
	@Override
	public void setUp() {
		BFLogger.logInfo("Step 2 - check if the table displayed: " + challengingDomPage.isTablePresent());
		assertTrue("There is no table", challengingDomPage.isTablePresent());
	}
	
	@Override
	public void tearDown() {
		
	}
	
	@Test
	@Parameters(method = "headerParameters")
	public void checkHeadersCorrectness(int column, String headerElement) {
		BFLogger.logInfo("Step 3 - check if the table header has correct elements");
		assertTrue("The table header has incorrect element", challengingDomPage.getHeaderTableValues()
				.get(column)
				.equals(headerElement));
	}
	
	private Object[] headerParameters() {
		return new Object[] {
						new Object[] { 0, "Lorem" },
						new Object[] { 1, "Ipsum" },
						new Object[] { 2, "Dolor" },
						new Object[] { 3, "Sit" },
						new Object[] { 4, "Amet" },
						new Object[] { 5, "Diceret" },
						new Object[] { 6, "Action" },
		};
	}
	
	@Test
	@Parameters(method = "columnParameters")
	public void check4thColumnCorrectness(int row, String columnElement) {
		BFLogger.logInfo("Step 3 - check if the table column has correct elements ");
		assertTrue("The table column has incorrect element", challengingDomPage.get4thColumnTableValues()
				.get(row)
				.equals(columnElement));
	}
	
	private Object[] columnParameters() {
		return new Object[] {
						new Object[] { 0, "Definiebas0" },
						new Object[] { 1, "Definiebas1" },
						new Object[] { 2, "Definiebas2" },
						new Object[] { 3, "Definiebas3" },
						new Object[] { 4, "Definiebas4" },
						new Object[] { 5, "Definiebas5" },
						new Object[] { 6, "Definiebas6" },
						new Object[] { 7, "Definiebas7" },
						new Object[] { 8, "Definiebas8" },
						new Object[] { 9, "Definiebas9" },
		};
	}
}
